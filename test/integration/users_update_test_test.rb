require 'test_helper'

class UsersUpdateTestTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "Redirect to user_path when update action is called directly" do
    patch user_path(@user)
    assert_redirected_to login_path
    log_in_as(@user)
    # patchで直接アクセスされた場合はuser_pathへ誘導する
    assert_redirected_to @user
  end

end
